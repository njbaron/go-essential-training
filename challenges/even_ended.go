/*  Challenge 2
 *  String challenge
 */

package main

import "fmt"

func main() {

	evenNumCount := 0

	for i := 1000; i <= 9999; i++ {
		for j := i; j <= 9999; j++ {
			value := i * j
			valueStr := fmt.Sprintf("%d", value)

			if valueStr[0] == valueStr[len(valueStr)-1] {
				evenNumCount += 1
			}
		}
	}

	fmt.Printf("Even Number Count: %d\n", evenNumCount)
}
