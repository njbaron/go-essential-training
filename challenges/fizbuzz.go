/*  Challenge 1
 *  Conditionals and for loop challenge
 */

package main

import (
	"fmt"
)

func main() {
	for i := 1; i < 21; i++ {

		three := i%3 > 0
		five := i%5 > 0

		switch {
		case !three && !five:
			fmt.Println("fizz buzz")
		case !three:
			fmt.Println("fizz")
		case !five:
			fmt.Println("buzz")
		default:
			fmt.Println(i)
		}
	}
}
