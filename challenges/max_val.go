/*  Challenge 3
 *  Slice Challenge
 */

package main

import (
	"fmt"
)

func main() {
	nums := []int{16, 8, 42, 4, 23, 15}

	max := nums[0] // Initialize to the first value because values might be negative.
	for _, val := range nums[1:] {

		if val > max {
			max = val
		}
	}

	fmt.Println(max)
}
