/*  Challenge 4
 *  Map challenge Count the number of words that show in the text.
 */

package main

import (
	"fmt"
	"strings"
)

func main() {

	text := "Needles and pins\nNeedles and pins\nSew me a sail\nTo catch me the wind"

	counts := map[string]int{}

	text = strings.ToLower(text)

	for _, word := range strings.Fields(text) {
		counts[word] += 1
	}

	fmt.Println(counts)

}
