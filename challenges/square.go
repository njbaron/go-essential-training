/*  Challenge 6
 *  Struct Challenge
 */

package main

import (
	"fmt"
	"log"
)

type Point struct {
	X int
	Y int
}

func (p *Point) Move(dx int, dy int) {
	p.X += dx
	p.Y += dy
}

type Square struct {
	Center Point
	Length int
}

func (sq *Square) Move(dx int, dy int) {
	sq.Center.Move(dx, dy)
}

func (sq *Square) Area() int {
	return sq.Length * sq.Length
}

func NewSquare(x int, y int, length int) (*Square, error) {

	if length < 0 {
		return nil, fmt.Errorf("length cannot be less than zero")
	}

	center := Point{
		X: x,
		Y: y,
	}

	square := &Square{
		Center: center,
		Length: length,
	}

	return square, nil

}

func main() {

	sq, err := NewSquare(3, 5, 10)
	if err != nil {
		log.Fatalln("Cannot create sqaure")
	}

	fmt.Println(sq)

	sq.Move(1, 4)

	fmt.Println(sq)
}
