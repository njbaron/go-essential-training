/*  Challenge 5
 *  Function and error challenge
 */

package main

import (
	"fmt"
	"net/http"
)

func contentType(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	} else {
		defer resp.Body.Close()
	}
	ctype := resp.Header.Get("Content-Type")
	if ctype == "" {
		return "", fmt.Errorf("Unable to get content type header")
	} else {
		return ctype, nil
	}
}

func main() {

	str, err := contentType("https://golang.org")
	if err != nil {
		fmt.Println("error! " + err.Error())
	} else {
		fmt.Println(str)
	}

}
